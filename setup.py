from setuptools import setup, find_packages

setup(
    name='rockpaper',  # Required
    version='1.0.0',  # Required
    url='https://gitlab.com/JTreneL/kamen_nuzky_papir_hra',  # Optional
    author='JTrenel',  # Optional
    author_email='Janlenert@email.cz',  # Optional
    packages=find_packages(),  # Required
    entry_points={"console_scripts": ["rockpaper = rockpaper.rockpaper:main"]},
)
