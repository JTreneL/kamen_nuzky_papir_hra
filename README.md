# Rock,paper,Scissors game 
- user vs computer 

# Install

- extract all files to new directory
- open console and go to directory
- enter ./install.sh


# Uninstall

- open console and go to directory
- enter ./uninstall.sh


# How to Run it:

- open console
- enter [username@fedora ~]$ rockpaper


# License:

- opensource