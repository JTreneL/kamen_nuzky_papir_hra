import random
import sys



def main():
    while True:
        možnosti_počítače = ["Kámen","Nůžky","Papír"]
        akce_počítače = random.choice(možnosti_počítače)
        hráčova_volba = input("Vyber si (Kámen,Nůžky,Papír): ")
        
        print(f"\nVybral sis {hráčova_volba}, Počítač si zvolil {akce_počítače}.\n")
        
        if hráčova_volba == akce_počítače:
            print("Zvolily jste stejně")
        
        elif hráčova_volba == "Nůžky":
            if akce_počítače == "Papír":
                print("Hráč roztříhal papír.")
            else:
                print("Ḱámen rozmlátil nůžky hráče.")
        
        elif hráčova_volba == "Kámen":
            if akce_počítače =="Nůžky":
                print("Hráč rozbil nůžky.")
            else:
                print("Počítač zabalil kámen hráče.")
        
        elif hráčova_volba =="Papír":
            if akce_počítače =="Kámen":
                print("Papír zabalil kámen hráč vyhrál.")
            else:
                print("Počítač rozstříhal papír hráče.")

        play_again = input("Hrát znova? (a/n): ")
        if play_again.lower() != "a":
            break

if __name__ == "__main__":
    sys.exit(main())
